from nltk.tag import StanfordPOSTagger
import collections
from google.cloud import firestore
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
cred = credentials.Certificate("/Users/edgaralejandroramirezaguilera/cred.json")
app = firebase_admin.initialize_app(cred)
store = firestore.client()
def processCommand(order):

    doc_ref = store.collection(u'focos')
    focos=order[0]
    verbos=order[1]
    segundos=order[2]
    for x in range(0,len(order[0])):
        focos_ref = doc_ref.document(order[0][x])
        if order[1][x]=="prender":
            focos_ref.set({
                'estado':True
            })
        else:
            focos_ref.set({
                'estado':False
            })
        focos_ref.set({
            'segundos':order[2][x]
        },merge=True)              
def defCommand(tweet):
    tagger="/Users/edgaralejandroramirezaguilera/Downloads/SistDist/stanford-postagger-full-2018-10-16/models/spanish.tagger"
    jar="/Users/edgaralejandroramirezaguilera/Downloads/SistDist/stanford-postagger-full-2018-10-16/stanford-postagger.jar"
    etiquetador=StanfordPOSTagger(tagger,jar)
    etiquetas=etiquetador.tag(tweet.split())
    d = collections.defaultdict(list)
    for v, k in etiquetas:
        d[k].append(v)
    verbos=d['vmn0000']
    focos=d['aq0000']
    segundos=d['z0']
    orden=[focos,verbos,segundos]
    processCommand(orden)


