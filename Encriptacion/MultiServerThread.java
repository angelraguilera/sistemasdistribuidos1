import java.net.*;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class MultiServerThread extends Thread {
    private Socket socket = null;

    public MultiServerThread(Socket socket) {
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient.NoClients++;
    }
    public static String encrypt(String strClearText,String strKey) throws Exception{
        String strData="";
    
        try {
            SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes(),"Blowfish");
            Cipher cipher=Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted=cipher.doFinal(strClearText.getBytes());
            strData=new String(encrypted);
    
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }
    public static String decrypt(String strEncrypted,String strKey) throws Exception{
        String strData="";
    
        try {
            SecretKeySpec skeyspec=new SecretKeySpec(strKey.getBytes(),"Blowfish");
            Cipher cipher=Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);
            byte[] decrypted=cipher.doFinal(strEncrypted.getBytes());
            strData=new String(decrypted);
    
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
        return strData;
    }
    public void run() {

        try {
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;

            while ((lineIn = entrada.readLine()) != null) {
                System.out.println("Received: " + lineIn.substring(1, 3));
                escritor.flush();
                char[] c = lineIn.toCharArray();
                if (c[0] == '#') {
                    lineIn = lineIn.replace("&", "");
                    String[] str = lineIn.split(":");
                    if (c[1] == '3' && c[2] == '2') {
                        String arlist = new String();
                        try{
                        arlist = encrypt(str[1],str[2]);
                        }
                        catch(Exception e){
                            System.out.println(e.toString());
                        }
                        String resp="R-"+c[1]+c[2]+":"+arlist;
                        String r = resp;
                        escritor.println(r+"&");
                    } 
                    else if(c[1] == '3' && c[2] == '3'){
                        String arlist = new String();
                        try{
                        arlist = decrypt(str[1],str[2]);
                        }
                        catch(Exception e){
                            System.out.println(e.toString());
                        }
                        String resp="R-"+c[1]+c[2]+":"+arlist;
                        String r = resp;
                        escritor.println(r+"&");
                    }
                  
                    else {
                        escritor.println("Servicio no disponible");
                        socket.close();
                    }
                    escritor.flush();
                } else {
                    ServerMultiClient.NoClients--;
                    socket.close();
                    break;
                }
            }
            try {
                entrada.close();
                escritor.close();
                socket.close();
            } catch (Exception e) {
                System.out.println("Error : " + e.toString());
                socket.close();
                System.exit(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
