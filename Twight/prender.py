import RPi.GPIO as GPIO
import time
from google.cloud import firestore
import google.cloud.exceptions
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import time
cred = credentials.Certificate("/Users/edgaralejandroramirezaguilera/cred.json")
app = firebase_admin.initialize_app(cred)
store = firestore.client()
def inicializar(led):
    GPIO.setup(led,GPIO.OUT)
def encender(color,tiempo):
    rojos = [4,17,18,27,22]
    azules = [23,24,9,25,8]
    amarillos = [7,5,6,12,13]
    verdes = [19,16,26,20,21]
    if color == "rojos":
        map(inicializar,rojos)
    elif color == "verdes":
        map(inicializar,verdes)
    elif color == "azules":
        map(inicializar,azules)
    elif color == "amarillos":
        map(inicializar,amarillos)
    for led in color:
        GPIO.output(led,True) 
    time.sleep(tiempo)
    GPIO.cleanup()
def on_snapshot(col_snapshot, changes, read_time):
    for doc in col_snapshot:
        print(u'{}'.format(doc.__dict__['_data']['seconds']))
        tiempo = int(doc.__dict__['_data']['seconds'])
        encender(doc.id,tiempo)
GPIO.setmode(GPIO.BCM)
col_query = store.collection(u'focos').where(u'state', u'==', True)
query_watch = col_query.on_snapshot(on_snapshot)
query_watch.unsubscribe()



# Watch the collection query

