from nltk.tag import StanfordPOSTagger
import collections
from google.cloud import firestore
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
cred = credentials.Certificate("/Users/edgaralejandroramirezaguilera/cred.json")
app = firebase_admin.initialize_app(cred)
store = firestore.client()
def processCommand(order):
    doc_ref = store.collection(u'focos')
    
    for x in range(0,len(order[0])):
        focos_ref = doc_ref.document(order[0][x])
        if order[1][x]=="prender":
            focos_ref.set({
                'state':True
            })
        else:
            focos_ref.set({
                'state':False
            })
        focos_ref.set({
            'seconds':order[2][x]
        },merge=True)              
def defCommand(tweet):
    print("holoooo")
    tagger="../stanford-postagger-full-2018-10-16/models/spanish.tagger"
    jar="../stanford-postagger-full-2018-10-16/stanford-postagger.jar"
    etiquetador=StanfordPOSTagger(tagger,jar)
    etiquetas=etiquetador.tag(tweet.split())
    d = collections.defaultdict(list)
    for v, k in etiquetas:
        d[k].append(v)
    verbos=d['vmn0000']
    focos=d['aq0000']
    segundos=d['z0']
    orden=[focos,verbos,segundos]
    rojos = [7,11,12,13,15]
    azules = [16,18,21,22,24]
    amarillos = [26,29,31,32,33]
    verdes = [35,36,37,38,40]
    processCommand(orden)


