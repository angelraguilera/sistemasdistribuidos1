from nltk.tag import StanfordPOSTagger
import collections
from google.cloud import firestore
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use the application default credentials

def processCommand(order):

    cred = credentials.Certificate("/Users/edgaralejandroramirezaguilera/cred.json")
    app = firebase_admin.initialize_app(cred)
    store = firestore.client()
    doc_ref = store.collection(u'focos')
    print(order)
def defCommand():
    tagger="/Users/edgaralejandroramirezaguilera/Downloads/SistDist/stanford-postagger-full-2018-10-16/models/spanish.tagger"
    jar="/Users/edgaralejandroramirezaguilera/Downloads/SistDist/stanford-postagger-full-2018-10-16/stanford-postagger.jar"
    etiquetador=StanfordPOSTagger(tagger,jar)
    etiquetas=etiquetador.tag("prender todos los focos rojos y apagar todos los focos amarillos por 10 segundos".split())
    #print(etiquetas)
    d = collections.defaultdict(list)
    for v, k in etiquetas:
        d[k].append(v)
    verbos=d['vmn0000']
    focos=d['aq0000']
    segundos=d['z0']
    orden=[verbos,focos,segundos]
    processCommand(orden)
defCommand()


