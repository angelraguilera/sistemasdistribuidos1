import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MultiServerThread extends Thread {
    private Socket socket = null;

    public MultiServerThread(Socket socket) {
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient.NoClients++;
    }

    public ArrayList sortString(String[] str, int n) {
        ArrayList<String> arlist = new ArrayList<String>();
        String sorted = null;
        char[] chars = null;
        for (int i = 2; i <= n + 1; i++) {
            chars = str[i].toCharArray();
            Arrays.sort(chars);
            sorted = new String(chars);
            arlist.add(sorted);
        }
        return arlist;
    }

    public void run() {

        try {
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;

            while ((lineIn = entrada.readLine()) != null) {
                System.out.println("Received: " + lineIn.substring(1, 3));
                escritor.flush();
                char[] c = lineIn.toCharArray();
                if (c[0] == '#') {
                    lineIn = lineIn.replace("&", "");
                    String[] str = lineIn.split(":");
                    if (c[1] == '2' && c[2] == '3') {
                        int numStr = Integer.parseInt(str[1]);
                        ArrayList<String> arlist = new ArrayList<String>();
                        arlist = sortString(str, numStr);
                        String resp="R-"+c[1]+c[2]+":"+arlist.size();
                        String r = resp;
                        for(int i=0;i<=arlist.size()-1;i++){
                            r=r.concat(":");
                            r=r.concat(arlist.get(i).toString());
                        }
                        escritor.println(r+"&");
                    } else {
                        escritor.println("Servicio no disponible");
                        socket.close();
                    }
                    escritor.flush();
                } else {
                    ServerMultiClient.NoClients--;
                    socket.close();
                    break;
                }
            }
            try {
                entrada.close();
                escritor.close();
                socket.close();
            } catch (Exception e) {
                System.out.println("Error : " + e.toString());
                socket.close();
                System.exit(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
